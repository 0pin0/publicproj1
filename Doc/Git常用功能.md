[Back主功能目錄]()

# **流程概述(Git常用功能)**

描述Git常用功能. 



# **目錄**

定義

***

建立client的版本庫 <br>
下載最後版本 <br>
上傳版本 <br>
回復版本 <br>

***




# **定義**

未受版控的檔
<pre>
non track file
跟版控無關的檔，會被clean up清除，但不會被reset清除
</pre>

從版控中忽略的檔
<pre>
git ingore file
上傳/下載時會忽略這些檔案
</pre>

已修改未commit檔案
<pre>
在.git目錄中記錄已變更，但沒commit
若失手修改刪除，無法救回，建議commit local branch
</pre>

client版本庫
<pre>
存在.git目錄中
commit local branch時會暫存在此
pull時會將server版本庫暫存在此
誤改檔案時，可用此救回
</pre>

server版本庫
<pre>
存在192的server上
在push時上傳到server
版本永久有效，不會被刪除，只會註記
用pull下載到client版本庫
</pre>

[目錄](http://192.168.0.2:14014/enable-ets/docs/blob/master/pinchen%E6%96%87%E4%BB%B6/%E5%AF%B5%E7%89%A9%E9%A3%BC%E6%96%99.md#%E7%9B%AE%E9%8C%84)
***
<br><br>



# 建立client的版本庫

<pre>
在server版本庫，選擇project後
按右上clone，選http，會複製一個網址
在檔案總管，建立project的目錄
在目錄右鍵git clone，貼上網址，專案目錄可以去掉一層
有指定branch時，可以輸入branch名(大小寫不同)
確定後下載project
</pre>

[目錄](http://192.168.0.2:14014/enable-ets/docs/blob/master/pinchen%E6%96%87%E4%BB%B6/%E5%AF%B5%E7%89%A9%E9%A3%BC%E6%96%99.md#%E7%9B%AE%E9%8C%84)
***
<br><br>



# 下載最後版本

<pre>
在檔案總管，選擇專案目錄
可先rar備份變更的檔。
若不用保留舊檔，用check difference，revert修改的檔，可避免衝突。
按右鍵pull專案，若有衝突要去解決
</pre>

[目錄](http://192.168.0.2:14014/enable-ets/docs/blob/master/pinchen%E6%96%87%E4%BB%B6/%E5%AF%B5%E7%89%A9%E9%A3%BC%E6%96%99.md#%E7%9B%AE%E9%8C%84)
***
<br><br>



# 上傳版本

<pre>
在檔案總管，選擇專案目錄
可先rar備份變更的檔。
用check difference，再確認一次修改的檔。
請同事code review修改的檔。
將修改的檔commit到 client版本庫。
按右鍵pull最後版本到 client版本庫 ，若有衝突要去解決。
按右鍵pushclient版本庫 的commit檔案 到 server版本庫。
開啟192的git網頁，選擇project的history確認上傳成功。
</pre>

[目錄](http://192.168.0.2:14014/enable-ets/docs/blob/master/pinchen%E6%96%87%E4%BB%B6/%E5%AF%B5%E7%89%A9%E9%A3%BC%E6%96%99.md#%E7%9B%AE%E9%8C%84)
***
<br><br>



# 回復版本

找回舊版本中，正確的檔案
<pre>
在檔案總管，選擇專案目錄
用check difference，revert修改的檔，回復版本時不能有修改中的檔案。
按右鍵git show log
在需要回復的版本，按右鍵 (第6個)"reset master(分支名) to this.."，倒退 client版本庫 的版號。
開unity確認版本正確。
將目標檔案備到到 專案外目錄(ex:桌面)。
</pre>

[下載最後版本](#下載最後版本)

在最後版本測試正確的檔案
<pre>
在檔案總管，選擇專案目錄
用check difference，確認沒有修改中的檔案。
按右鍵git show log，確認和 server版本庫 相同，是最新版本。
將備份在 專案外目錄 的檔案，copy到專案中
開unity確認版本正確。
</pre>

[上傳版本](#上傳版本)

[目錄](http://192.168.0.2:14014/enable-ets/docs/blob/master/pinchen%E6%96%87%E4%BB%B6/%E5%AF%B5%E7%89%A9%E9%A3%BC%E6%96%99.md#%E7%9B%AE%E9%8C%84)
***
<br><br>



[Back主功能目錄]()

# END
